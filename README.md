GameDevWeek
===========

https://fsi.hochschule-trier.de/dev/null/GameDevWeek  
https://fsi.hochschule-trier.de/dev/null/Vampire_next_Door  

Download:  
https://cloud.fsi.hochschule-trier.de/index.php/s/0OoX4K4wUubt9gA  
https://bitbucket.org/geht/vampirenextdoor_gdw-ss-16-a/downloads/  

some important links:  
https://fsi.hochschule-trier.de/dev/null/GameDevWeek/Programmieren  
https://fsi.hochschule-trier.de/dev/null/GameDevWeek/Eclipse  
https://fsi.hochschule-trier.de/dev/null/GameDevWeek/Maven  
https://github.com/GameDevWeek/CodeBase/wiki/Used-technologies  